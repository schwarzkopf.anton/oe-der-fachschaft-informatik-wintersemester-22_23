# OE der Fachschaft Informatik Wintersemester 22/23

Hallo liebe Informatikstudierende!

Dieses Repo ist eine Sammlung von Infos, die wir mal während der coronabedingten
Uni-Schließung angefangen haben, aktualisiert für das Wintersemester 22/23.
Vielleicht ist ja was dabei, was dir durch den am Anfang undurchdringlich
erscheinenden Uni-Dschungel hilft (das ist ganz normal und wird schon noch
mit der Zeit).

Eine Frage ist trotz der ganzen Texte untergegangen? Du möchtest Mitstudierende,
Fachschaftler*innen und/oder Studierende aus höheren Semestern kennen lernen?
Dann komm doch in unsere 

* **Telegram Gruppe:** https://t.me/informatikinfo

oder komm auf unseren

* **Discord Server:** https://discord.gg/ttRpw25

Irgendjemand wird da schon online sein und ein offenes Ohr für deine Fragen haben.
Außerdem lässt sich so bestimmt auch jemand finden, der einen Übungszettel gemeinsam
machen möchte.

Du hast weder Telegram, noch Discord oder willst uns bei Fragen lieber per Mail
kontaktieren? Schreib uns eine **Mail** über das Kontaktformular auf unserer Webseite: 

* https://fs.cs.uni-frankfurt.de/contact#

Unser auf **Twitter** könnt ihr uns auch folgen (bitte keine Fragen wegen Hilfe dort):

* @fachschaftinfo

Außerdem empfehlen wir den Discord Channel des Ingo-Wegener-Lernzentrums, der zusätzlich
aktuelle Informationen zu den meisten Basismodulen bereitstellt. Siehe dazu die Seite
des Lernzentrums: 
* http://www.cs.uni-frankfurt.de/index.php/de/service-fuer-studierende/lernzentrum.html

Der Vollständigkeit halber möchten wir noch auf zwei Facebook-Gruppen hinweisen, wo wir
sporadisch noch Veranstaltungsankündigungen für Social Events der Fachschaft Informatik
verbreiten, ansonsten aber zur Zeit nicht weiter nutzen:

* https://www.facebook.com/groups/goethe.informatik/
* https://www.facebook.com/FSInf.Frankfurt


# Was sind diese ganzen Dokumente da?
Ganz einfach:
* **Don't Panic 42:**   Unsere Erstsemesterzeitschrift. Die wird normalerweise in Print-Form 
                        während unserer Orientierungsveranstaltung (auch OE genannt)
                        herausgegeben. Nunja. Die ist leider ins Wasser gefallen, soll
                        aber noch nachgeholt werden. Die Don't Panic gibt es aber trotzdem
                        schonmal hier. Sie beinhaltet in Kurzform alles wichtige, was ihr
                        zu eurem Studium wissen solltet. Ein Blick lohnt sich also!
* **Vortragsfolien Erstes Semester:**
                        Empfohlene Basismodule im ersten Semester und Auge System zur Einteilung
                        in die Übungsgruppen in den Basismodulen der Informatik.
* **IT-Plattformen:**   Eine Zusammenfassung aller wichtigen Online Systeme unserer Uni.
                        Wie funktioniert das QIS oder die E-Learning Plattformen (z.B. OLAT, Moodle)?
                        Was bietet die RBI an und wie kann ich meine Uni-Mailadresse nutzen oder
                        was ist eigentlich das WLAN eduroam sind nur ein paar der Fragen, die hier 
                        (kurz) beantwortet werden.
* **Tipps und Tricks:** Eine Sammlung von verschiedenen Erkenntnissen zum Studieren, 
                        die wir Fachschaftler*innen in unserer unendlichen Weisheit (hust)
                        über die Jahre angesammelt haben.
* **Anwendungsfach Mathe**
                        Über die Wahl eines Anwendungsfachesmüsst ihr Euch in den ersten 
                        Semestern eigentlich keine Gedanken machen - außer ihr wollt Mathe
                        wählen. Dann solltet ihr Euch gut überlegen, ob ihr statt der
                        Mathevorlesungen in der Informatik nicht lieber gleich die aus der
                        Mathe macht, um zu vermeiden, dass ihr später Vertiefungs- statt
                        Basisveranstaltungen in der Mathe machen müsst. Dieses PDF wurde aus
                        der Infor dazu von der Webseite der Informatik generiert, Quelle
                        siehe im PDF)
* **Anmeldung zum Bachelorprüfungsverfahren**
                        Bevor ihr Euch zur ersten Prüfung anmelden könnt, muss die Anmeldung
                        zum Prüfungsverfahren ausgefüllt und beim Prüfungsamt im Original
                        abgegeben werden. Sonst wundert ihr Euch, warum eine Anmeldung zur
                        Klausur nicht möglich ist (und müsst dann schnell mit dem 
                        Prüfungsamt Kontakt aufnehmen). Dieses Dokument gibt es online
                        zum Runterladen beim Prüfungsamt Informatik. Für Aktualität 
                        übernehmen wir keine Gewähr, das PDF ist Stand 12.10.2022)
                        PS: im Master gibt es ebenfalls eine Anmedlung zum Prüfungsverfahren.



